package com.thirtyjune.networkservice

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class CellularNetworkTest {
    @Test
    fun testSilentAuthMobile() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext

        val networkService = NetworkService(appContext)
        networkService
            .cellularNetwork()
            .request()

        InstrumentationRegistry.getInstrumentation().waitForIdle {
            val mobileAvailable = networkService.isAvailable
            assertEquals(true, mobileAvailable)
        }
    }
}