package com.thirtyjune.networkservice

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.util.Log

class NetworkService(context: Context) : NetworkCallback() {
    companion object {
        const val TAG = "Network Cellular"
    }

    private var networkRequest: NetworkRequest? = null
    private var callbackNetwork: CallbackNetworkService? = null

    private val connectivityManager by lazy {
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }
    
    var isAvailable:Boolean = false

    /**
     * Setup network has Internet and use Cellular Network
     *
     * @return CellularNetworkService
     */
    fun cellularNetwork(): NetworkService {
        networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()

        return this
    }

    /**
     * Set request network to connectivity manager
     * @return void
     */
    fun request() {
        connectivityManager.requestNetwork(networkRequest!!, this)
    }

    /**
     * Used to check if Mobile Data Settings is On/Off
     * @return Boolean
     */
    fun isMobileDataEnabled(): Boolean {
        val cmClass = Class.forName(connectivityManager.javaClass.name)
        val method = cmClass.getDeclaredMethod("getMobileDataEnabled")
        method.isAccessible = true

        // Get the setting for "mobile data"
        return method.invoke(connectivityManager) as Boolean
    }

    /**
     * Callback when Network Available
     * @return void
     */
    override fun onAvailable(network: Network) {
        Log.d(TAG, "Available Network")

        isAvailable = true

        when {
            // Check if Android Version is Marshmallow or Newer
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                connectivityManager.bindProcessToNetwork(network)
            }
            else -> {
                @Suppress("DEPRECATION") ConnectivityManager.setProcessDefaultNetwork(network)
            }
        }

        callbackNetwork?.onAvailable()
    }

    /**
     * Callback when Network Lost
     * @return void
     */
    override fun onLost(network: Network) {
        Log.d(TAG, "Lost Network")
        isAvailable = false

        callbackNetwork?.onLost()
    }
}