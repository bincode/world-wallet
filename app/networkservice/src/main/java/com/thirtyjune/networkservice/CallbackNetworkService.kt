package com.thirtyjune.networkservice

interface CallbackNetworkService {
    fun onAvailable(){}
    fun onLost(){}
}