package com.thirtyjune.walletworld

import android.app.Activity
import android.content.Intent
import android.util.Base64
import com.thirtyjune.walletworld.activity.*

class Routes(private val activity: Activity) {
    private val intent = Intent()

    fun toLogin(): Routes {
        intent.setClass(activity, LoginActivity::class.java)
        return this
    }

    fun toVerificationOTP(bundle: Any?): Routes {

        if (bundle == null) {
            return toMainMenu()
        }

        intent.setClass(activity, VerificationOneTimePasswordActivity::class.java)
        intent.putExtra("verification_otp", bundle.toString())
        return this
    }

    fun toMainMenu(): Routes {
        /* Clear Previous Activity */
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        /* RedirectTo if does`t have menu */
        val config = Preference.config
        if (config.hasMenu == 0) {
            return toSilentAuth()
        }

        intent.setClass(activity, MenuActivity::class.java)
        return this
    }

    fun toSilentAuth(): Routes {
        intent.setClass(activity, SilentAuthActivity::class.java)
        return this
    }

    fun toStatus(message: String, log: String? = null): Routes {
        intent.setClass(activity, StatusActivity::class.java)

        if (log != null) {
            val decodedBytes = Base64.decode(log, Base64.DEFAULT)
            val decodedString = String(decodedBytes)
            intent.putExtra("log", decodedString)
        }
        intent.putExtra("message", message)

        return this
    }

    fun startActivity(isOutsideActivity: Boolean = false): Activity {
        if (isOutsideActivity) intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(intent)

        return activity
    }
}