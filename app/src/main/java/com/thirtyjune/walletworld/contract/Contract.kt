package com.thirtyjune.walletworld.contract

interface Contract {
    fun handleIfSuccess(bundle: Any? = null, log: String? = null) { /* compiled code */ }
    fun showProsesMessage(msg: String? = null) { /* compiled code */ }
    fun showFailedMessage(msg: String? = null) { /* compiled code */ }
    fun showErrorMessage(msg: String? = null) { /* compiled code */ }
}