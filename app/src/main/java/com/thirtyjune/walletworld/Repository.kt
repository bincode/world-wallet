package com.thirtyjune.walletworld

import android.content.Context
import com.thirtyjune.walletworld.api.APIClient
import com.thirtyjune.walletworld.api.APIInterface
import com.thirtyjune.walletworld.callback.*
import com.thirtyjune.walletworld.contract.Contract

@Suppress("DEPRECATION")
class Repository(private val context: Context) {
    private var apiInterface: APIInterface = APIClient.getClient().create(APIInterface::class.java)

    fun login(email: String, password: String, contract: Contract) {
        apiInterface.login(email, password).enqueue(LoginResponseCallback(contract))
    }

    fun resendCode(uid:String,contract: Contract) {
        apiInterface.resendCode(uid).enqueue(ResendCodeCallback(contract))
    }

    fun sendMSISDN(msisdn: String, credential: String, contract: Contract) {
        apiInterface.sendMSISDN(getUID(), msisdn, credential).enqueue(SilentAuthResponseCallback(context, contract))
    }

    fun validationUrl(correlationId: String, url: String, contract: Contract) {
        apiInterface.hitValidationUrl(url).enqueue(ValidationResultCallback(context, correlationId, contract))
    }

    fun sendResult(correlationID: String, result: String, contract: Contract) {
        apiInterface.sendAuthResult(getUID(), correlationID, result).enqueue(AuthResultCallback(context, correlationID, contract))
    }

    fun checkStatus(correlationId: String, contract: Contract) {
        apiInterface.checkStatus(getUID(), correlationId).enqueue(AuthStatusCallback(context, correlationId, contract))
    }

    private fun getUID(): String {
        return Preference.account.uid
    }
}