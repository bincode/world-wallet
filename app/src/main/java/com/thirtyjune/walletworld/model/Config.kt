package com.thirtyjune.walletworld.model

import com.thirtyjune.walletworld.response.Credential
import com.thirtyjune.walletworld.response.Menu

data class Config(val hasMenu: Int, val menuList : ArrayList<Menu>, val credentialList : ArrayList<Credential>)