package com.thirtyjune.walletworld.api

import android.net.Network
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class APIClient {
    companion object {
        private var baseUrl: String = "http://202.157.176.129/"
        private var retrofit: Retrofit? = null

        fun getClient(network: Network? = null): Retrofit {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)

            if (network != null) {
                client.socketFactory(network.socketFactory)
            }

            if (retrofit == null) retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client.build())
                .build()

            return retrofit!!
        }
    }
}