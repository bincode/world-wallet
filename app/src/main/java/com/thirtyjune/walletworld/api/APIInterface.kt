package com.thirtyjune.walletworld.api

import com.thirtyjune.walletworld.response.*
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {

    @FormUrlEncoded
    @POST("ap_login.php")
    fun login(
        @Field("mail") email: String,
        @Field("pass") password: String,
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("ap_otp.php")
    fun resendCode(
        @Field("uid") uid: String,
    ): Call<ResendCodeResponse>

    @FormUrlEncoded
    @POST("ap_geturl.php")
    fun sendMSISDN(
        @Field("uid") uid: String,
        @Field("msisdn") msisdn: String,
        @Field("credential") credential: String,
    ): Call<SilentAuthResponse>

    @FormUrlEncoded
    @POST("ap_urlresult.php")
    fun sendAuthResult(
        @Field("uid") uid: String,
        @Field("corrid") correlationID: String,
        @Field("result") result: String,
    ): Call<AuthResultResponse>

    @FormUrlEncoded
    @POST("ap_result.php")
    fun checkStatus(
        @Field("uid") uid: String,
        @Field("corrid") correlationID: String,
    ): Call<AuthStatusResponse>

    @GET
    fun hitValidationUrl(
        @Url url: String,
    ): Call<String>
}