package com.thirtyjune.walletworld

import com.thirtyjune.walletworld.model.Account
import com.thirtyjune.walletworld.model.Config

object Preference {
    var account: Account = Account("")
    var config: Config = Config(0, arrayListOf(), arrayListOf())
}