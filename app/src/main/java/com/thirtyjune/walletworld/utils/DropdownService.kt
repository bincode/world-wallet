package com.thirtyjune.walletworld.utils

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Spinner

class DropdownService(private val context: Context) {

    fun setup(view: Spinner, list: ArrayList<*>): Spinner {

        // Fill data in Spinner
        val adapter: ArrayAdapter<Any> = ArrayAdapter(
            context,
            android.R.layout.simple_dropdown_item_1line,
            list
        )

        return view.apply {
            this.adapter = adapter
        }
    }
}