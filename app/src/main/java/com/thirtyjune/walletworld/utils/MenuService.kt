package com.thirtyjune.walletworld.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.thirtyjune.walletworld.R
import com.thirtyjune.walletworld.response.Menu

class MenuService(context: Context, layout: ViewGroup, list: ArrayList<Menu>, listener: View.OnClickListener) {
    //private val layout: ViewGroup = context.findViewById<LinearLayout>(R.id.menuLayout)

    init {
        /* Generate Menu */
        for (menu in list) {

            /* Get Layout Inflate */
            val input = LayoutInflater
                .from(context)
                .inflate(R.layout.menu, layout, false) as TextView

            input.apply {
                text = menu.name
                setOnClickListener(listener)
                /*setOnClickListener {
                    when (menu.name) {
                        "PNV" -> Routes(activity).toSilentAuth()
                            .startActivity()
                    }
                }*/
            }
            layout.addView(input)
        }
    }
}