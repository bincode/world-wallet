package com.thirtyjune.walletworld.utils

object Stopwatch {

     private var startTime:Long = 0

     fun start(){
          startTime = System.currentTimeMillis()
     }

     fun stop():Long{
          return System.currentTimeMillis() - startTime
     }
}