package com.thirtyjune.walletworld.activity

import android.os.Bundle
import android.os.CountDownTimer
import android.text.format.DateUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.thirtyjune.otplayout.OTPLayout
import com.thirtyjune.otplayout.OnMatchListener
import com.thirtyjune.walletworld.R
import com.thirtyjune.walletworld.Repository
import com.thirtyjune.walletworld.Routes
import com.thirtyjune.walletworld.Preference
import com.thirtyjune.walletworld.contract.Contract

class VerificationOneTimePasswordActivity : AppCompatActivity(), OnMatchListener, Contract {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification_one_time_password)

        /* Hide Action Bar */
        supportActionBar?.hide()

        setupUI()
    }

    /**
     * Function to Setup UI
     * Prepare initialize event ui element
     */
    private fun setupUI() {
        /* Button Verification OTP */
        findViewById<Button>(R.id.btnVerify).setOnClickListener {
            val value: String? = intent.getStringExtra("verification_otp")
            findViewById<OTPLayout>(R.id.otp_layouts).setOnMatch(value, this)
        }

        /* Button Press Back */
        findViewById<ImageView>(R.id.btnBackVerifyOTP).setOnClickListener {
            this.onBackPressed()
        }

        /* Button Resend Code OTP */
        findViewById<TextView>(R.id.btnResendCode).apply {
            object : CountDownTimer(1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    val second = DateUtils.formatElapsedTime(millisUntilFinished / 1000)
                    text = resources.getString(R.string.countdown, (second).toString())
                    setOnClickListener(null)
                }

                override fun onFinish() {
                    text = resources.getString(R.string.resend)
                    setOnClickListener {
                        this.start()

                        val activity = this@VerificationOneTimePasswordActivity
                        Repository(activity).resendCode(Preference.account.uid, activity)
                    }
                }
            }.start()
        }
    }

    /**
     * Triggered when user inputted correct OTP
     * @Event
     */
    override fun onMatch() {
        Routes(this).toMainMenu().startActivity().finish()
    }

    /**
     * Triggered when user inputted wrong OTP
     * @Event
     */
    override fun onFailed() {
        findViewById<TextView>(R.id.otpErrorMessage).apply {
            text = resources.getString(R.string.error_otp)
        }
    }

    override fun handleIfSuccess(bundle: Any?, log: String?) {
        intent.putExtra("verification_otp", bundle as String)
    }

    override fun onBackPressed() {
        Routes(this).toLogin().startActivity().finish()
    }
}