package com.thirtyjune.walletworld.activity

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.hbb20.CountryCodePicker
import com.thirtyjune.networkservice.NetworkService
import com.thirtyjune.walletworld.R
import com.thirtyjune.walletworld.Repository
import com.thirtyjune.walletworld.Routes
import com.thirtyjune.walletworld.utils.Stopwatch
import com.thirtyjune.walletworld.Preference
import com.thirtyjune.walletworld.contract.Contract
import com.thirtyjune.walletworld.utils.DropdownService

class SilentAuthActivity : AppCompatActivity(), Contract {
    private var networkService: NetworkService = NetworkService(this)
    private lateinit var progressDialog:AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_silent_auth)

        setupActionBar()
        setupUI()

        networkService.cellularNetwork().request()
    }

    /**
     * Function to Setup Action Bar
     * Prepare initialize event action bar element
     */
    private fun setupActionBar() {
        val config = Preference.config
        supportActionBar?.apply {
            /* Tell if Display with Custom View  */
            displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM

            /* Set Custom View */
            setCustomView(R.layout.action_bar)
            setDisplayHomeAsUpEnabled(config.hasMenu == 1)

            /* Set Title in Custom View */
            customView.findViewById<TextView>(R.id.action_bar_title).apply {
                text = resources.getString(R.string.title_silent_auth)
            }
        }
    }

    /**
     * Function to Setup UI
     * Prepare initialize event UI element
     */
    private fun setupUI() {
        val config = Preference.config

        /* Dropdown Credential */
        val dropdownService = DropdownService(this)
        dropdownService.setup(findViewById(R.id.selectCredential), config.credentialList)

        /* Alert Dialog Mobile Network Failed */
        val alertMobileNetworkOff = AlertDialog.Builder(this)
            .setTitle("Network Failed")
            .setMessage("This application requires a mobile network, Please turn on your mobile data")
            .setNegativeButton("Close", DialogInterface.OnClickListener { dialogInterface, _ ->
                dialogInterface.dismiss()
            })

        /* Progress Dialog */
        @Suppress("DEPRECATION")
        progressDialog = ProgressDialog(this).apply {
            setMessage("Please wait.. (\"1 / 4\")")
        }

        /* Button Authenticate */
        findViewById<Button>(R.id.btnAuthenticate).apply {
            setOnClickListener {
                if (!networkService.isMobileDataEnabled()) {
                    alertMobileNetworkOff.show()
                    return@setOnClickListener
                }

                /* Start Timer to calculate how long process it takes */
                Stopwatch.start()

                val activity = this@SilentAuthActivity

                /* Input Phone Number */
                val inputPhone = activity.findViewById<EditText>(R.id.inputPhone)

                /* Country Picker */
                val ccp = activity.findViewById<CountryCodePicker>(R.id.ccp).apply {
                    registerCarrierNumberEditText(inputPhone)
                    setNumberAutoFormattingEnabled(true)
                }

                /* Dropdown Credential */
                val credentialID = activity.findViewById<Spinner>(R.id.selectCredential).selectedItemId
                val credentialValue = config.credentialList[credentialID.toInt()].creVal
                val phoneNumber = ccp.formattedFullNumber

                if (credentialValue != null) {
                    progressDialog.show()
                    Repository(activity).sendMSISDN(phoneNumber, credentialValue, this@SilentAuthActivity)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun handleIfSuccess(bundle: Any?, log: String?) {
        progressDialog.dismiss()

        Routes(this).toStatus(bundle.toString(), log)
            .startActivity()
            .finish()
    }

    override fun showFailedMessage(msg: String?) {
        progressDialog.dismiss()

        Routes(this).toStatus(msg.toString())
            .startActivity()
            .finish()
    }

    override fun showErrorMessage(msg: String?) {
        Toast.makeText(this, "Network Error", Toast.LENGTH_LONG).show()
    }

    override fun showProsesMessage(msg: String?) {
        progressDialog.setMessage(msg)
        Log.d("AuthProcess", msg.toString())
    }
}