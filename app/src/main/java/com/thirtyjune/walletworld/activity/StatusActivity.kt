package com.thirtyjune.walletworld.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.thirtyjune.walletworld.R
import com.thirtyjune.walletworld.Routes
import com.thirtyjune.walletworld.utils.Stopwatch
import org.json.JSONException
import org.json.JSONObject

class StatusActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)

        // Hide Action Bar
        supportActionBar?.hide()

        setupUI()
    }

    /**
     * Function to Setup UI
     * Prepare initialize event UI element
     */
    private fun setupUI() {
        val message = intent.getStringExtra("message") ?: "Authenticated"

        /* Image Status */
        val imageStatus = if (message != "Authenticated") R.drawable.ic_error else R.drawable.ic_success
        findViewById<ImageView>(R.id.imageStatus).apply {
            setImageResource(imageStatus)
        }

        /* Label Message */
        findViewById<TextView>(R.id.labelMessage).apply {
            text = message
        }

        /* Timer Stop */
        findViewById<TextView>(R.id.msg_time).apply {
            text = resources.getString(R.string.time_taken, Stopwatch.stop().toString())
        }

        /* Error Log */
        val log = intent.getStringExtra("log")
        findViewById<TextView>(R.id.msg_error).apply {
            if (log != null) {
                text = beautifyJson(log)
            } else {
                visibility = View.GONE
            }
        }

        /* Button Back To Menu */
        findViewById<Button>(R.id.btnMenu).setOnClickListener{
            Routes(this).toMainMenu().startActivity().finish()
        }
    }

    /**
     * Beautify formated JSON String
     * @param string String
     * @return String
     */
    private fun beautifyJson(string: String) : String{
        return try {
            val logJSONObject = JSONObject(string)
            logJSONObject.toString(4)
        } catch (e: JSONException) {
            string
        }
    }
}