package com.thirtyjune.walletworld.activity

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.thirtyjune.walletworld.R
import com.thirtyjune.walletworld.Repository
import com.thirtyjune.walletworld.Routes
import com.thirtyjune.walletworld.contract.Contract

class LoginActivity : AppCompatActivity(), Contract {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // Turn off Night Mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        findViewById<Button>(R.id.btnLogin).apply {
            setOnClickListener(processLogin)
        }
    }

    private val processLogin = View.OnClickListener {
        val email = findViewById<EditText>(R.id.inputEmail).text.toString()
        val password = findViewById<EditText>(R.id.inputPassword).text.toString()

        Repository(this@LoginActivity).login(email, password, this)
    }

    override fun handleIfSuccess(bundle: Any?, log: String?) {
        Routes(this).toVerificationOTP(bundle).startActivity().finish()
    }

    override fun showFailedMessage(msg: String?) {
        findViewById<TextView>(R.id.credentialErrorMessage).apply {
            text = resources.getString(R.string.error_credential)
        }
    }

    override fun showErrorMessage(msg: String?) {
        val message = resources.getString(R.string.error_network)
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}