package com.thirtyjune.walletworld.activity

import android.os.Bundle
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.thirtyjune.walletworld.R
import com.thirtyjune.walletworld.Routes
import com.thirtyjune.walletworld.Preference
import com.thirtyjune.walletworld.utils.MenuService

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        setupMenu()
    }

    /**
     * Function to Setup UI
     * Prepare initialize event ui element
     */
    private fun setupMenu(){
        val layout: ViewGroup = findViewById<LinearLayout>(R.id.menuLayout)

        Preference.config.run {

            /* Set Menu */
            MenuService(this@MenuActivity, layout, menuList) {
                val route = Routes(this@MenuActivity)

                /* Event Menu */
                when ((it as TextView).text) {
                    "PNV" -> route.toSilentAuth().startActivity()
                }
            }
        }
    }
}