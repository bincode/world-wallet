package com.thirtyjune.walletworld.response

import com.google.gson.annotations.SerializedName

class ResendCodeResponse {
    @SerializedName("otp")
    var OTP: String = ""
}
