package com.thirtyjune.walletworld.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SilentAuthResponse : Serializable {
    @SerializedName("uid")
    var uid: String = ""

    @SerializedName("is_url")
    var hasUrl: Int? = 0

    @SerializedName("url")
    var url: String = ""

    @SerializedName("corrid")
    var correlationID: String = ""

    @SerializedName("stat")
    var state: String = ""
}
