package com.thirtyjune.walletworld.response

import com.google.gson.annotations.SerializedName

class AuthStatusResponse {
    @SerializedName("stat")
    var status: String? = null

    @SerializedName("log")
    var log: String? = null
}
