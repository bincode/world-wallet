package com.thirtyjune.walletworld.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Menu : Serializable {
    @SerializedName("name")
    var name: String? = null
}
