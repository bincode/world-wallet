package com.thirtyjune.walletworld.response

import com.google.gson.annotations.SerializedName

class AuthResultResponse {
    @SerializedName("stat")
    var status: String? = null

    @SerializedName("delay")
    var delay: Int? = null
}
