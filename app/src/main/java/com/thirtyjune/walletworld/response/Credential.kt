package com.thirtyjune.walletworld.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Credential : Serializable {
    @SerializedName("cre_name")
    var name: String? = null

    @SerializedName("cre_value")
    var creVal: String? = null

    override fun toString(): String {
        return this.name!! // What to display in the Spinner list.
    }
}
