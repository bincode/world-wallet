package com.thirtyjune.walletworld.response

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("uid")
    var uid: String = ""

    @SerializedName("token")
    var token: String? = null

    @SerializedName("is_login")
    var isLogin: Int? = 0

    @SerializedName("is_otp")
    var isOtp: Int? = 0

    @SerializedName("otp")
    var OTP: String = ""

    @SerializedName("is_menu")
    var isMenu: Int = 0

    @SerializedName("list_menu")
    var menuList: ArrayList<Menu> = arrayListOf()

    @SerializedName("list_credential")
    var credentialList: ArrayList<Credential> = arrayListOf()
}
