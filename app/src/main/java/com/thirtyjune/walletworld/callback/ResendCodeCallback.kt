package com.thirtyjune.walletworld.callback

import android.util.Log
import com.thirtyjune.walletworld.contract.Contract
import com.thirtyjune.walletworld.response.ResendCodeResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResendCodeCallback(private val contract: Contract) : Callback<ResendCodeResponse> {
    override fun onResponse(call: Call<ResendCodeResponse>, response: Response<ResendCodeResponse>) {
        Log.i("Callback", "Login Response Callback")
        val resource: ResendCodeResponse = response.body() ?: return

        contract.handleIfSuccess(resource.OTP)
    }

    override fun onFailure(call: Call<ResendCodeResponse>, t: Throwable) {
        contract.showErrorMessage(t.message)
    }
}
