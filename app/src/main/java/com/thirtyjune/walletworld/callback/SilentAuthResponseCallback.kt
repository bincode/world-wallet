package com.thirtyjune.walletworld.callback

import android.content.Context
import android.util.Log
import com.thirtyjune.walletworld.Repository
import com.thirtyjune.walletworld.contract.Contract
import com.thirtyjune.walletworld.response.SilentAuthResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SilentAuthResponseCallback(val context: Context, val contract: Contract) : Callback<SilentAuthResponse?> {

    override fun onResponse(call: Call<SilentAuthResponse?>, response: Response<SilentAuthResponse?>) {
        Log.i("Callback", "Silent Auth Response Callback")
        val resource: SilentAuthResponse? = response.body()

        if (resource == null) {
            contract.showFailedMessage("No response when Sending MSISDN")
            return
        }

        if (resource.hasUrl == 1) {
            contract.showProsesMessage("Please wait.. (\"2 / 4\")")
            Repository(context).validationUrl(resource.correlationID, resource.url, contract)
        } else {
            contract.showFailedMessage(resource.state)
        }
    }

    override fun onFailure(call: Call<SilentAuthResponse?>, t: Throwable) {
        contract.showErrorMessage(t.message)
        t.printStackTrace()
    }
}

