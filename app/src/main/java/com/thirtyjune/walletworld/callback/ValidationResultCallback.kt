package com.thirtyjune.walletworld.callback

import android.content.Context
import android.util.Base64
import android.util.Log
import com.thirtyjune.walletworld.Repository
import com.thirtyjune.walletworld.contract.Contract
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ValidationResultCallback(val context: Context, private val correlationID: String, val contract: Contract) : Callback<String?> {
    override fun onResponse(call: Call<String?>, response: Response<String?>) {
        Log.i("Callback", "Validation Result Callback")
        val resource: String? = response.body()

        if (resource == null) {
            contract.showFailedMessage("No response when Sending Result to Server")
            return
        }

        /* Encode Result */
        val data: ByteArray = resource.toByteArray()
        val resBase64 = Base64.encodeToString(data, Base64.DEFAULT)

        contract.showProsesMessage("Please wait.. (\"3 / 4\")")
        Repository(context).sendResult(correlationID, resBase64, contract)
    }

    override fun onFailure(call: Call<String?>, t: Throwable) {
        contract.showErrorMessage(t.message)
        t.printStackTrace()
    }
}

