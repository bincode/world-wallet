package com.thirtyjune.walletworld.callback

import android.content.Context
import android.util.Log
import com.thirtyjune.walletworld.Repository
import com.thirtyjune.walletworld.contract.Contract
import com.thirtyjune.walletworld.response.AuthResultResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthResultCallback(val context: Context, val correlationID: String, private val contract: Contract): Callback<AuthResultResponse?> {
    override fun onResponse(call: Call<AuthResultResponse?>, response: Response<AuthResultResponse?>) {
        Log.i("Callback", "Auth Result Callback")
        val resource: AuthResultResponse? = response.body()

        if (resource == null) {
            contract.showFailedMessage("Sending Authentication Result to Server")
            return
        }

        if (resource.status != "OK") {
            contract.showFailedMessage("Authentication has status '${resource.status}'")
            return
        }

        contract.showProsesMessage("Please wait.. (\"3 / 4\")")
        Repository(context).checkStatus(correlationID, contract)
    }

    override fun onFailure(call: Call<AuthResultResponse?>, t: Throwable) {
        contract.showErrorMessage(t.message)
        t.printStackTrace()
    }
}
