package com.thirtyjune.walletworld.callback

import android.content.Context
import android.util.Log
import com.thirtyjune.walletworld.Repository
import com.thirtyjune.walletworld.contract.Contract
import com.thirtyjune.walletworld.response.AuthStatusResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.concurrent.schedule

class AuthStatusCallback(val context: Context, var correlationID: String, val contract: Contract) : Callback<AuthStatusResponse?> {
    companion object{
        var attemptCount: Int = 0
    }

    override fun onResponse(call: Call<AuthStatusResponse?>, response: Response<AuthStatusResponse?>) {
        Log.i("Callback", "Auth Status Callback")
        val resource: AuthStatusResponse? = response.body()

        if (resource == null) {
            contract.showFailedMessage("No response when Checking Status")
            return
        }

        contract.showProsesMessage("Please wait.. (\"4 / 4\")")

        when (resource.status) {
            "match" -> {
                attemptCount = 0
                contract.handleIfSuccess("Authenticated", resource.log)
            }
            "" -> {
                contract.showProsesMessage("Rechecking Status (${attemptCount})")
                attemptCount++

                Timer().schedule(2000) {
                    Repository(context).checkStatus(correlationID, contract)
                }
            }
            null -> {
                attemptCount++
                Timer().schedule(2000) {
                    Repository(context).checkStatus(correlationID, contract)
                }
            }
            else -> {
                attemptCount = 0
                contract.handleIfSuccess(resource.status, resource.log)
            }
        }
    }

    override fun onFailure(call: Call<AuthStatusResponse?>, t: Throwable) {
        contract.showErrorMessage(t.message)
        t.printStackTrace()
    }
}