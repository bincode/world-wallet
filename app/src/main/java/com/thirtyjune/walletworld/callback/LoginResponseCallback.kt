package com.thirtyjune.walletworld.callback

import android.util.Log
import com.thirtyjune.walletworld.Preference
import com.thirtyjune.walletworld.contract.Contract
import com.thirtyjune.walletworld.model.Account
import com.thirtyjune.walletworld.model.Config
import com.thirtyjune.walletworld.response.LoginResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginResponseCallback(private val contract: Contract) : Callback<LoginResponse> {

    override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
        Log.i("Callback", "Login Response Callback")
        val resource: LoginResponse = response.body() ?: return

        /* Check Login Success */
        if (resource.isLogin == 0) { // Failed
            contract.showFailedMessage()
            return
        }

        /* Put Preference Data */
        Preference.apply {
            config = Config(resource.isMenu, resource.menuList, resource.credentialList)
            account = Account(resource.uid)
        }

        /* Check has OTP */
        if (resource.isOtp == 1) {

            contract.handleIfSuccess(resource.OTP)
            return
        }

        /* Check has Menu */
        if (resource.isMenu == 1) {
            contract.handleIfSuccess()
            return
        }

        contract.handleIfSuccess()
    }

    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
        contract.showErrorMessage(t.message)
    }
}
