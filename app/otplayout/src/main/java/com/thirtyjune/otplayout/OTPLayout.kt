package com.thirtyjune.otplayout

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.widget.doOnTextChanged

class OTPLayout(context: Context, attrs: AttributeSet?): LinearLayout(context, attrs) {
    private var textLength: Int

    init {
        /* Bind Custom Style Attributes */
        context.obtainStyledAttributes(attrs, R.styleable.OTPLayout, 0, 0).apply {
            textLength = getInteger(R.styleable.OTPLayout_textLength, 5)
        }.recycle()

        /* Create Box Number */
        for (i in 1..textLength) {
            this.addView(createBoxNumber(i))
        }
    }

    /**
     * Function to create box otp number
     *
     * @param index Integer
     * @return EditText
     */
    private fun createBoxNumber(index:Int?): EditText {
        /* Get Layout Inflate */
        val input = LayoutInflater
            .from(context)
            .inflate(R.layout.input_otp, this, false) as EditText

        input.apply{
            /* Do Next Focus when Text Change */
            doOnTextChanged { _, _, _, _ ->
                if ((index ?: textLength) < textLength && this.text.isNotEmpty()) {
                    this.focusSearch(View.FOCUS_RIGHT).requestFocus()
                }
            }
            setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) this.text.clear()
            }
        }
        return input
    }

    /**
     * Get Input text on this layout
     *
     * @return String
     */
    private fun getText(): String {
        val stringBuilder = StringBuilder()

        val count = this.childCount - 1
        for (i in 0..count) {
            val view: View = this.getChildAt(i)
            if (view !is EditText) continue

            val input = view.text
            stringBuilder.append(input)
        }

        return stringBuilder.toString()
    }

    fun setOnMatch(value: String?, listener: OnMatchListener) {
        if(getText() == value){
            listener.onMatch()
        }else{
            listener.onFailed()
        }
    }
}